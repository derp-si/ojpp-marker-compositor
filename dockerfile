FROM lukemathwalker/cargo-chef:latest-rust-1.67.0 as chef

WORKDIR /app

FROM chef as planner

COPY Cargo.lock Cargo.toml ./
# Compute a lock-like file for our project
RUN cargo chef prepare  --recipe-path recipe.json

FROM chef as builder

RUN apt-get update -y
RUN apt-get install -y --no-install-recommends \
    openssl \
	clang \
	curl \
	g++-9 \
	gcc \
	git \
	libfontconfig1-dev \
	libgl1 \
	libgl1-mesa-dev \
	libgles2-mesa-dev \
	libssl-dev \
	mesa-common-dev \
	pkg-config \
	python \
	ninja.build


COPY Cargo.lock Cargo.toml ./
COPY crates /app/crates

COPY --from=planner /app/recipe.json recipe.json

RUN cargo chef cook --release --recipe-path recipe.json


# Build our project
RUN cargo build --release --locked --bin server 


FROM debian:bullseye-slim AS runtime

RUN apt-get update -y \
	&& apt-get install -y --no-install-recommends libfontconfig1-dev \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /app
COPY --from=builder /app/target/release/server /app/server

ENV APP_ENVIRONMENT=production

CMD ["/app/server"]
