use axum::{extract::{Query, State}, response::IntoResponse};
use serde::{Serialize, Deserialize};
use compositor::compositions::render_marker;
use http::{header, StatusCode};
use skia_safe::{EncodedImageFormat, Surface};

use crate::state::ServerState;

#[derive(Serialize, Deserialize)]
pub struct ComposeQuery {
    pub size: u32,
}

pub async fn compose(State(state): State<ServerState>,query: Query<ComposeQuery>) -> impl IntoResponse {
    let size = query.size;

    if size > state.max_composition_size {
        return (
            StatusCode::BAD_REQUEST,
            format!("Size must be less than or equal to {}.", state.max_composition_size),
        )
            .into_response()
    }

    let mut surface =
        Surface::new_raster_n32_premul((size as i32, size as i32)).expect("No SKIA surface available.");

    render_marker(size as f32, surface.canvas());

    let image = surface.image_snapshot();
    let encoded_image = match image.encode_to_data(EncodedImageFormat::PNG) {
        Some(data) => data.as_bytes().to_owned(),
        None => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to encode image to PNG.",
            )
                .into_response()
        }
    };

    (
        axum::response::AppendHeaders([
            (header::CONTENT_TYPE, "image/png"),
            (header::CACHE_CONTROL, "public, max-age=31536000"),
        ]),
        encoded_image,
    )
        .into_response()
}
