use std::net::SocketAddr;

#[derive(clap::Parser)]
pub struct Arguments {
    #[clap(long, env, default_value = "0.0.0.0:8080")]
    pub bind_address: SocketAddr,
    
    #[clap(long, env, default_value = "32")]
    pub max_composition_size: u32,
}

impl std::fmt::Display for Arguments {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "bind_address: {}", self.bind_address)?;
        writeln!(f, "max_composition_size: {}", self.max_composition_size)?;

        Ok(())
    }
}
